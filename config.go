package main

type config struct {
	Listen     string `env:"LISTEN" envDefault:":8080"`
	StaticPath string `env:"STATIC_PATH" envDefault:"static"`
}
