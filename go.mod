module gitlab.com/orchids/aerides

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
)
