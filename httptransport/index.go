package httptransport

import "net/http"

// index writes content of default landing page
func index(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`<!doctype html>

<html lang="en">

  <head>
    <meta charset="utf-8">

    <title>Landing Page</title>
    <meta name="description" content="Landing page">
    <meta name="author" content="Jakub Daliga">
  </head>

  <body>
    <h1>Hello, World!</h1>
  </body>

</html>`))
}
