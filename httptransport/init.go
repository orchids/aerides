package httptransport

import (
	"net/http"

	"github.com/go-chi/chi"
)

// Init initializes HTTP routing within application
func Init(
	staticPath string,
) http.Handler {
	r := chi.NewRouter()

	r.Get("/", index)

	r.Get("/*", staticFile(staticPath))

	return r
}
