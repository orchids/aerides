package httptransport

import (
	"net/http"
	"path"
	"path/filepath"
)

var (
	allowedExtensions = map[string]struct{}{
		".ico":  struct{}{},
		".html": struct{}{},
		".css":  struct{}{},
		".js":   struct{}{},
	}
)

// staticFile serves static file requested by user
func staticFile(staticPath string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		extension := filepath.Ext(r.URL.Path)
		if !allowedExtension(extension) {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		dir, filename := path.Split(r.URL.Path)
		if dir != "/" {
			r.URL.Path = "/" + filename
			http.Redirect(w, r, r.URL.String(), http.StatusPermanentRedirect)
			return
		}

		http.ServeFile(w, r, staticPath+"/"+filename)
	}
}

// allowedExtension returns if given extension should be returned from static
// server
func allowedExtension(extension string) bool {
	_, ok := allowedExtensions[extension]
	return ok
}
