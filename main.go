package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/caarlos0/env"
	"gitlab.com/orchids/aerides/httptransport"
)

func main() {
	var conf config
	if err := env.Parse(&conf); err != nil {
		log.Fatalf("parse config: %v", err)
	}

	fmt.Println("Starting server on", conf.Listen)
	http.ListenAndServe(conf.Listen, httptransport.Init(conf.StaticPath))
}
